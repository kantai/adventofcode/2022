package main

import (
	"fmt"
	"os"
)

func hasDuplicates(symbols []byte) bool {
	alreadyIncluded := make(map[byte]bool)
	for _, symbol := range symbols {
		if alreadyIncluded[symbol] {
			return true
		} else {
			alreadyIncluded[symbol] = true
		}
	}
	return false
}

func main() {

	inputFile, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer inputFile.Close()

	lastSymbols := make([]byte, 0)
	buffer := make([]byte, 1)
	pos := 0

	for true {
		read, err := inputFile.Read(buffer)
		if read == 0 {
			break
		}
		if err != nil {
			panic(err)
		}
		pos += read

		currentSymbol := buffer[0]
		lastSymbols = append(lastSymbols, currentSymbol)

		if len(lastSymbols) > 4 && !hasDuplicates(lastSymbols[len(lastSymbols)-4:]) {
			fmt.Printf("Found packet marker at position %d\n", pos)
		}
		if len(lastSymbols) > 14 && !hasDuplicates(lastSymbols[len(lastSymbols)-14:]) {
			fmt.Printf("Found message marker at position %d\n", pos)
			break
		}
	}
}
