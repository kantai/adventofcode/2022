package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Field struct {
	StartingPoint bool
	VisitedTail   bool
	VisitedHead   bool
}

type Grid struct {
	Fields [][]*Field
	Head   *Knot
	Tail   []*Knot
}

type Knot struct {
	X int
	Y int
}

func (g *Grid) Move(move Move) {
	if move.Direction == Left && g.Head.X == 0 {
		for rowIndex, row := range g.Fields {
			newRow := make([]*Field, len(row)+1)
			for i := 0; i < len(row); i++ {
				newRow[i+1] = row[i]
			}
			newRow[0] = &Field{}
			g.Fields[rowIndex] = newRow
		}
		g.Head.X = 1
		for _, tail := range g.Tail {
			tail.X++
		}
	} else if move.Direction == Down && g.Head.Y == 0 {
		oldRows := g.Fields
		newRows := make([][]*Field, len(oldRows)+1)
		for i := 0; i < len(oldRows); i++ {
			newRows[i+1] = oldRows[i]
		}
		newRow := make([]*Field, len(g.Fields[0]))
		for i := 0; i < len(g.Fields[0]); i++ {
			newRow[i] = &Field{}
		}
		newRows[0] = newRow
		g.Fields = newRows
		g.Head.Y = 1
		for _, tail := range g.Tail {
			tail.Y++
		}
	} else if move.Direction == Right && g.Head.X == len(g.Fields[0])-1 {
		for rowIndex, row := range g.Fields {
			newRow := make([]*Field, len(row)+1)
			for i := 0; i < len(row); i++ {
				newRow[i] = row[i]
			}
			newRow[len(g.Fields[rowIndex])] = &Field{}
			g.Fields[rowIndex] = newRow
		}
	} else if move.Direction == Up && g.Head.Y == len(g.Fields)-1 {
		oldRows := g.Fields
		newRows := make([][]*Field, len(oldRows)+1)
		for i := 0; i < len(oldRows); i++ {
			newRows[i] = oldRows[i]
		}
		newRow := make([]*Field, len(g.Fields[0]))
		for i := 0; i < len(g.Fields[0]); i++ {
			newRow[i] = &Field{}
		}
		newRows[len(g.Fields)] = newRow
		g.Fields = newRows
	}

	if move.Direction == Left {
		g.Head.X -= 1
	} else if move.Direction == Right {
		g.Head.X += 1
	} else if move.Direction == Down {
		g.Head.Y -= 1
	} else if move.Direction == Up {
		g.Head.Y += 1
	}
	g.Fields[g.Head.Y][g.Head.X].VisitedHead = true

	previousTail := g.Head
	for _, tail := range g.Tail {
		tail.MoveTail(previousTail)
		previousTail = tail
	}
	lastTail := len(g.Tail) - 1
	g.Fields[g.Tail[lastTail].Y][g.Tail[lastTail].X].VisitedTail = true

	remainingSteps := move.Steps - 1
	if remainingSteps > 0 {
		g.Move(Move{Direction: move.Direction, Steps: remainingSteps})
	}
}

func (k *Knot) MoveTail(previousKnot *Knot) {
	diffX := previousKnot.X - k.X
	diffY := previousKnot.Y - k.Y
	if diffX > 1 {
		if diffY >= 1 {
			k.Y += 1
		} else if diffY <= -1 {
			k.Y -= 1
		}
		k.X += 1
	} else if diffX < -1 {
		if diffY >= 1 {
			k.Y += 1
		} else if diffY <= -1 {
			k.Y -= 1
		}
		k.X -= 1
	} else if diffY > 1 {
		if diffX >= 1 {
			k.X += 1
		} else if diffX <= -1 {
			k.X -= 1
		}
		k.Y += 1
	} else if diffY < -1 {
		if diffX >= 1 {
			k.X += 1
		} else if diffX <= -1 {
			k.X -= 1
		}
		k.Y -= 1
	}
}

func (g *Grid) DrawGrid() {
	fmt.Println("-------------------------------------------------------")
	for y := len(g.Fields) - 1; y >= 0; y-- {
		for x := 0; x < len(g.Fields[y]); x++ {
			if x == g.Head.X && y == g.Head.Y {
				fmt.Print("H")
				continue
			}
			foundTail := false
			for tailIndex, tail := range g.Tail {
				if x == tail.X && y == tail.Y {
					fmt.Print(strconv.Itoa(tailIndex + 1))
					foundTail = true
					break
				}
			}
			if foundTail {
				continue
			}
			if g.Fields[y][x].VisitedTail {
				fmt.Print("#")
			} else if g.Fields[y][x].VisitedHead {
				fmt.Print("o")
			} else {
				fmt.Print(".")
			}
		}
		fmt.Println("")
	}
}

type Direction string

const (
	Up    Direction = "U"
	Down            = "D"
	Left            = "L"
	Right           = "R"
)

type Move struct {
	Direction Direction
	Steps     int
}

func (m Move) String() string {
	return fmt.Sprintf("Move: %s * %d", m.Direction, m.Steps)
}

func ParseMove(line string) Move {
	parts := strings.Split(line, " ")
	if len(parts) != 2 {
		panic(fmt.Errorf("invalid move in input file: %s", line))
	}
	direction := Direction(parts[0])
	steps, err := strconv.Atoi(parts[1])
	if err != nil {
		panic(err)
	}
	return Move{Direction: direction, Steps: steps}
}

func main() {
	inputFile, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer inputFile.Close()

	grid := Grid{
		Fields: [][]*Field{[]*Field{&Field{VisitedTail: true, VisitedHead: true}}},
		Head:   &Knot{},
		Tail: []*Knot{
			&Knot{},
			&Knot{},
			&Knot{},
			&Knot{},
			&Knot{},
			&Knot{},
			&Knot{},
			&Knot{},
			&Knot{},
		},
	}
	grid.DrawGrid()
	scanner := bufio.NewScanner(inputFile)
	for scanner.Scan() {
		line := scanner.Text()
		move := ParseMove(line)
		fmt.Printf("%s\n", move)
		grid.Move(move)
		//grid.DrawGrid()
	}

	countTail := 0
	for y := 0; y < len(grid.Fields); y++ {
		for x := 0; x < len(grid.Fields[y]); x++ {
			if grid.Fields[y][x].VisitedTail {
				countTail++
			}
		}
	}
	fmt.Printf("The tail visited %d fields\n", countTail)
}
