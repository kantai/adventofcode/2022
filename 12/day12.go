package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

type Node struct {
	Location             string
	Height               int
	Neighbours           []*Node
	IsStart              bool
	IsEnd                bool
	MinimumNumberOfSteps int
}

func (n *Node) String() string {
	if n.IsStart {
		return "S"
	}
	if n.IsEnd {
		return "E"
	}
	return string(rune(n.Height))
}

func ParseRow(line string, row string) []*Node {
	var nodes []*Node
	for pos, symbol := range line {
		node := &Node{MinimumNumberOfSteps: 10000000, Location: row + strconv.Itoa(pos)}
		if symbol == 'S' {
			node.IsStart = true
			symbol = 'a'
		}
		if symbol == 'E' {
			node.IsEnd = true
			symbol = 'z'
		}
		node.Height = int(symbol)
		nodes = append(nodes, node)
	}
	return nodes
}

func FindEndNode(node *Node, step int) []*Node {
	if node.IsEnd {
		return []*Node{node}
	}
	if step >= node.MinimumNumberOfSteps {
		return nil
	}
	node.MinimumNumberOfSteps = step
	var minimumResult []*Node
	for _, neighbour := range node.Neighbours {
		result := FindEndNode(neighbour, step+1)
		if result != nil {
			newResult := make([]*Node, len(result)+1)
			for i, resultNode := range result {
				newResult[i+1] = resultNode
			}
			newResult[0] = node
			if minimumResult == nil || len(minimumResult) > len(newResult) {
				minimumResult = newResult
			}
		}
	}
	return minimumResult
}

func main() {
	inputFile, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer inputFile.Close()

	scanner := bufio.NewScanner(inputFile)
	grid := make([][]*Node, 0)
	row := 'A'
	for scanner.Scan() {
		line := scanner.Text()
		if line != "" {
			nodesInRow := ParseRow(line, string(row))
			grid = append(grid, nodesInRow)
		}
		row++
	}

	fmt.Printf("Grid:\n%s\n", grid)

	var startNode *Node
	//var endNode *Node

	for y := 0; y < len(grid); y++ {
		for x := 0; x < len(grid[y]); x++ {
			if x > 0 {
				if grid[y][x-1].Height <= grid[y][x].Height+1 {
					grid[y][x].Neighbours = append(grid[y][x].Neighbours, grid[y][x-1])
				}
			}
			if x < len(grid[y])-1 {
				if grid[y][x+1].Height <= grid[y][x].Height+1 {
					grid[y][x].Neighbours = append(grid[y][x].Neighbours, grid[y][x+1])
				}
			}
			if y > 0 {
				if grid[y-1][x].Height <= grid[y][x].Height+1 {
					grid[y][x].Neighbours = append(grid[y][x].Neighbours, grid[y-1][x])
				}
			}
			if y < len(grid)-1 {
				if grid[y+1][x].Height <= grid[y][x].Height+1 {
					grid[y][x].Neighbours = append(grid[y][x].Neighbours, grid[y+1][x])
				}
			}

			if grid[y][x].IsStart {
				startNode = grid[y][x]
			}
			//if grid[y][x].IsEnd {
			//	endNode = grid[y][x]
			//}
		}
	}
	pathToEnd := FindEndNode(startNode, 0)

	for _, node := range pathToEnd {
		fmt.Printf(" -> %s ", node.Location)
	}
	fmt.Println("")

	fmt.Printf("Found end node in %d Steps\n", len(pathToEnd)-1)

	// Part 2:
	var possibleStartNodes []*Node

	for y := 0; y < len(grid); y++ {
		for x := 0; x < len(grid[y]); x++ {
			if grid[y][x].Height == 'a' {
				possibleStartNodes = append(possibleStartNodes, grid[y][x])
			}
		}
	}
	minimumPathLength := 10000000
	for _, alternativeStartNode := range possibleStartNodes {
		pathToEnd := FindEndNode(alternativeStartNode, 0)
		if pathToEnd != nil && len(pathToEnd) < minimumPathLength {
			minimumPathLength = len(pathToEnd)
		}
	}
	fmt.Printf("Minimum path length %d Steps\n", minimumPathLength-1)

}
