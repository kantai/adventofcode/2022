package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

const maxGridWidth = 20
const maxGridHeight = 20
const maxGridDepth = 20

type Cube struct {
	FreeSides int
}

type Grid struct {
	Cubes [][][]*Cube
}

func NewGrid() *Grid {
	grid := Grid{}
	for z := 0; z < maxGridDepth; z++ {
		layer := make([][]*Cube, 0)
		for y := 0; y < maxGridHeight; y++ {
			row := make([]*Cube, maxGridWidth)
			layer = append(layer, row)
		}
		grid.Cubes = append(grid.Cubes, layer)
	}
	return &grid
}

func (g *Grid) AddCube(z int, y int, x int) *Cube {

	g.Cubes[z][y][x] = &Cube{FreeSides: 6}

	if x > 0 {
		if g.Cubes[z][y][x-1] != nil {
			g.Cubes[z][y][x-1].FreeSides--
			g.Cubes[z][y][x].FreeSides--
		}
	}
	if x < maxGridWidth-1 {
		if g.Cubes[z][y][x+1] != nil {
			g.Cubes[z][y][x+1].FreeSides--
			g.Cubes[z][y][x].FreeSides--
		}
	}

	if y > 0 {
		if g.Cubes[z][y-1][x] != nil {
			g.Cubes[z][y-1][x].FreeSides--
			g.Cubes[z][y][x].FreeSides--
		}
	}
	if y < maxGridHeight-1 {
		if g.Cubes[z][y+1][x] != nil {
			g.Cubes[z][y+1][x].FreeSides--
			g.Cubes[z][y][x].FreeSides--
		}
	}

	if z > 0 {
		if g.Cubes[z-1][y][x] != nil {
			g.Cubes[z-1][y][x].FreeSides--
			g.Cubes[z][y][x].FreeSides--
		}
	}
	if z < maxGridDepth-1 {
		if g.Cubes[z+1][y][x] != nil {
			g.Cubes[z+1][y][x].FreeSides--
			g.Cubes[z][y][x].FreeSides--
		}
	}
	return g.Cubes[z][y][x]
}

func CellIndex(z int, y int, x int) int {
	return z*10000 + y*100 + x
}

func GridCoordinates(cellIndex int) (int, int, int) {
	return cellIndex / 10000, (cellIndex % 10000) / 100, cellIndex % 100
}

func (g *Grid) FindEnclosedPockets() [][]int {
	pockets := make([][]int, 0)

	outsideCells := make(map[int]bool)
	insidePocketCells := make(map[int]bool)

	for z := 0; z < maxGridDepth; z++ {
		for y := 0; y < maxGridHeight; y++ {
			for x := 0; x < maxGridWidth; x++ {
				if x == 0 || y == 0 || z == 0 ||
					x == maxGridWidth-1 || y == maxGridHeight-1 || z == maxGridDepth-1 {
					outsideCells[CellIndex(z, y, x)] = true
				}
			}
		}
	}

	for z := 1; z < maxGridDepth-1; z++ {
		for y := 1; y < maxGridHeight-1; y++ {
			for x := 1; x < maxGridWidth-1; x++ {
				cellIndex := CellIndex(z, y, x)
				_, isInsidePocket := insidePocketCells[cellIndex]
				_, isOutside := outsideCells[cellIndex]
				if g.Cubes[z][y][x] == nil && !isOutside && !isInsidePocket {

					isPocket := true
					pocketCells := map[int]bool{}
					cellIndex := CellIndex(z, y, x)

					possiblePocketNeighbors := []int{cellIndex}

					for true {
						if len(possiblePocketNeighbors) == 0 {
							break
						}
						cellIndex := possiblePocketNeighbors[0]
						possiblePocketNeighbors = possiblePocketNeighbors[1:]
						_, alreadyInPocket := pocketCells[cellIndex]
						if alreadyInPocket {
							continue
						}
						cz, cy, cx := GridCoordinates(cellIndex)

						if g.Cubes[cz][cy][cx] == nil {
							pocketCells[cellIndex] = true

							_, exists := outsideCells[cellIndex]
							if exists {
								isPocket = false
							}

							if cx > 0 {
								neighbourCellIndex := CellIndex(cz, cy, cx-1)
								_, alreadyInPocket := pocketCells[neighbourCellIndex]
								if !alreadyInPocket {
									possiblePocketNeighbors = append(possiblePocketNeighbors, neighbourCellIndex)
								}
							}

							if cx < maxGridWidth-1 {
								neighbourCellIndex := CellIndex(cz, cy, cx+1)
								_, alreadyInPocket := pocketCells[neighbourCellIndex]
								if !alreadyInPocket {
									possiblePocketNeighbors = append(possiblePocketNeighbors, neighbourCellIndex)
								}
							}

							if cy > 0 {
								neighbourCellIndex := CellIndex(cz, cy-1, cx)
								_, alreadyInPocket := pocketCells[neighbourCellIndex]
								if !alreadyInPocket {
									possiblePocketNeighbors = append(possiblePocketNeighbors, neighbourCellIndex)
								}
							}

							if cy < maxGridHeight-1 {
								neighbourCellIndex := CellIndex(cz, cy+1, cx)
								_, alreadyInPocket := pocketCells[neighbourCellIndex]
								if !alreadyInPocket {
									possiblePocketNeighbors = append(possiblePocketNeighbors, neighbourCellIndex)
								}
							}

							if cz > 0 {
								neighbourCellIndex := CellIndex(cz-1, cy, cx)
								_, alreadyInPocket := pocketCells[neighbourCellIndex]
								if !alreadyInPocket {
									possiblePocketNeighbors = append(possiblePocketNeighbors, neighbourCellIndex)
								}
							}

							if cz < maxGridDepth-1 {
								neighbourCellIndex := CellIndex(cz+1, cy, cx)
								_, alreadyInPocket := pocketCells[neighbourCellIndex]
								if !alreadyInPocket {
									possiblePocketNeighbors = append(possiblePocketNeighbors, neighbourCellIndex)
								}
							}
						}
					}
					if !isPocket {
						for cellIndex, _ := range pocketCells {
							outsideCells[cellIndex] = true
						}
						fmt.Printf("Found outside %d\n", len(pocketCells))
					} else {
						pocket := make([]int, 0)
						for cellIndex, _ := range pocketCells {
							insidePocketCells[cellIndex] = true
							pocket = append(pocket, cellIndex)
						}
						pockets = append(pockets, pocket)
						fmt.Printf("Found pocket %d\n", len(pocketCells))
					}
				}
			}
		}
	}
	return pockets
}

func main() {
	inputFile, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer inputFile.Close()

	grid := NewGrid()
	allCubes := make([]*Cube, 0)
	scanner := bufio.NewScanner(inputFile)
	for scanner.Scan() {
		line := scanner.Text()
		cubeCoordinateStrings := strings.Split(line, ",")
		x, err := strconv.Atoi(cubeCoordinateStrings[0])
		if err != nil {
			panic(err)
		}
		y, err := strconv.Atoi(cubeCoordinateStrings[1])
		if err != nil {
			panic(err)
		}
		z, err := strconv.Atoi(cubeCoordinateStrings[2])
		if err != nil {
			panic(err)
		}
		cube := grid.AddCube(z, y, x)
		allCubes = append(allCubes, cube)
	}

	totalFreeSides := 0
	for _, cube := range allCubes {
		totalFreeSides += cube.FreeSides
	}

	fmt.Printf("Total number of free sides: %d\n", totalFreeSides)

	enclosedPockets := grid.FindEnclosedPockets()

	fmt.Printf("Enclosed pockets: %d\n", len(enclosedPockets))

	pocketGrid := NewGrid()
	var allPocketCubes []*Cube
	for _, pocket := range enclosedPockets {
		for _, cellIndex := range pocket {
			z, y, x := GridCoordinates(cellIndex)
			cube := pocketGrid.AddCube(z, y, x)
			allPocketCubes = append(allPocketCubes, cube)
		}
	}
	totalPocketSides := 0
	for _, cube := range allPocketCubes {
		totalPocketSides += cube.FreeSides
	}
	fmt.Printf("Free sides in pockets: %d\n", totalPocketSides)

	fmt.Printf("Total number of exterior free sides: %d\n", totalFreeSides-totalPocketSides)

}
