package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type SectionRange struct {
	Begin int
	End   int
}

func (sr SectionRange) Includes(otherSectionRange SectionRange) bool {
	return sr.Begin <= otherSectionRange.Begin && sr.End >= otherSectionRange.End
}

func (sr SectionRange) Overlaps(otherSectionRange SectionRange) bool {
	return !(sr.End < otherSectionRange.Begin || sr.Begin > otherSectionRange.End)
}

func (sr SectionRange) String() string {
	return fmt.Sprintf("%d-%d", sr.Begin, sr.End)
}

func ParseSectionRange(sectionRange string) SectionRange {
	sections := strings.Split(sectionRange, "-")
	begin, err := strconv.Atoi(sections[0])
	if err != nil {
		panic(err)
	}
	end, err := strconv.Atoi(sections[1])
	if err != nil {
		panic(err)
	}

	return SectionRange{
		Begin: begin,
		End:   end,
	}
}

func main() {
	inputFile, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}

	var totalOverlaps int
	var overlaps int
	scanner := bufio.NewScanner(inputFile)
	for scanner.Scan() {
		line := scanner.Text()

		ranges := strings.Split(line, ",")

		range1 := ParseSectionRange(ranges[0])
		range2 := ParseSectionRange(ranges[1])

		if range1.Includes(range2) || range2.Includes(range1) {
			totalOverlaps += 1
			fmt.Printf("Full overlap of sections %s and %s\n", range1, range2)
		}

		if range1.Overlaps(range2) || range2.Overlaps(range1) {
			overlaps += 1
			fmt.Printf("Overlap of sections %s and %s\n", range1, range2)
		}
	}
	fmt.Printf("Number of full overlap sections: %d\n", totalOverlaps)
	fmt.Printf("Number of partial overlap sections: %d\n", overlaps)
}
