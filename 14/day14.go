package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Voxel struct {
	IsRock  bool
	IsSand  bool
	IsAbyss bool
}

func (v *Voxel) IsFree() bool {
	return !v.IsRock && !v.IsSand
}

func (v *Voxel) TakeSand() {
	v.IsSand = true
}

type Cave struct {
	Voxels [][]*Voxel
}

func NewCave(width int, height int) *Cave {
	cave := Cave{}
	cave.Voxels = make([][]*Voxel, height)
	for y := 0; y < height; y++ {
		cave.Voxels[y] = make([]*Voxel, width)
		for x := 0; x < width; x++ {
			cave.Voxels[y][x] = &Voxel{}
		}
	}
	return &cave
}

func (c *Cave) AddFloor() {
	floor := make([]*Voxel, len(c.Voxels[0]))
	for x := 0; x < len(floor); x++ {
		floor[x] = &Voxel{IsRock: true}
	}
	c.Voxels = append(c.Voxels, floor)
}

func (c *Cave) GetVoxel(x int, y int) *Voxel {
	if y >= 0 && y < len(c.Voxels) {
		if x >= 0 && x < len(c.Voxels[y]) {
			return c.Voxels[y][x]
		}
	}
	return &Voxel{IsAbyss: true}
}

func (c *Cave) MarkRock(path []Position) {
	if len(path) < 2 {
		panic("Path missing second position")
	}
	for positionIndex := 1; positionIndex < len(path); positionIndex++ {
		srcPosition := path[positionIndex-1]
		destPosition := path[positionIndex]
		if srcPosition.Y == destPosition.Y {
			y := srcPosition.Y
			start := srcPosition.X
			end := destPosition.X
			if srcPosition.X > destPosition.X {
				start, end = end, start
			}
			for x := start; x <= end; x++ {
				c.Voxels[y][x].IsRock = true
			}
		} else if srcPosition.X == destPosition.X {
			x := srcPosition.X
			start := srcPosition.Y
			end := destPosition.Y
			if srcPosition.Y > destPosition.Y {
				start, end = end, start
			}
			for y := start; y <= end; y++ {
				c.Voxels[y][x].IsRock = true
			}
		} else {
			panic("No support for diagonal path segments")
		}
	}
}

func (c *Cave) DropSand(x int, y int) (bool, error) {
	dropped := false
	var err error
	directBelow := c.GetVoxel(x, y+1)
	if directBelow.IsAbyss {
		return false, fmt.Errorf("reached the abyss")
	}
	if directBelow.IsFree() {
		dropped, err = c.DropSand(x, y+1)
	}
	leftBelow := c.GetVoxel(x-1, y+1)
	if err == nil && !dropped && leftBelow.IsFree() {
		dropped, err = c.DropSand(x-1, y+1)
	}
	rightBelow := c.GetVoxel(x+1, y+1)
	if err == nil && !dropped && rightBelow.IsFree() {
		dropped, err = c.DropSand(x+1, y+1)
	}
	if err == nil && !dropped {
		c.GetVoxel(x, y).TakeSand()
		dropped = true
	}
	return dropped, err
}

func (c *Cave) String() string {
	s := ""
	for y := 0; y < len(c.Voxels); y++ {
		for x := 0; x < len(c.Voxels[y]); x++ {
			//if x > 480 {
			if c.Voxels[y][x].IsRock {
				s += fmt.Sprint("#")
			} else if c.Voxels[y][x].IsSand {
				s += fmt.Sprint("o")
			} else {
				s += fmt.Sprint(".")
			}
			//}
		}
		s += fmt.Sprintln("")
	}
	return s
}

func (c *Cave) Dump(filename string) {
	cave := c.String()
	err := os.WriteFile(filename, []byte(cave), 755)
	if err != nil {
		panic(err)
	}
}

type Position struct {
	X int
	Y int
}

func ParsePath(line string) ([]Position, error) {
	pathPositionStrings := strings.Split(line, " -> ")
	paths := make([]Position, len(pathPositionStrings))
	for index, positionString := range pathPositionStrings {
		coordinateStrings := strings.Split(positionString, ",")
		x, err := strconv.Atoi(coordinateStrings[0])
		if err != nil {
			return nil, err
		}
		y, err := strconv.Atoi(coordinateStrings[1])
		if err != nil {
			return nil, err
		}
		paths[index] = Position{X: x, Y: y}
	}
	return paths, nil
}

func main() {
	inputFile, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer inputFile.Close()

	scanner := bufio.NewScanner(inputFile)
	var paths [][]Position
	maxY := 0
	for scanner.Scan() {
		line := scanner.Text()
		path, err := ParsePath(line)
		for _, position := range path {
			if maxY < position.Y {
				maxY = position.Y
			}
		}
		paths = append(paths, path)
		fmt.Printf("Read path %s\n", path)
		if err != nil {
			panic(err)
		}
	}

	cave := NewCave(1000, maxY+2)
	for _, path := range paths {
		cave.MarkRock(path)
	}
	cave.AddFloor()

	fmt.Printf("Cave:\n%s", cave)

	for i := 1; i < 10000000; i++ {
		//fmt.Printf("Cave before step %d:\n%s", i, cave)
		fmt.Printf("Step %d:\n", i)
		_, err := cave.DropSand(500, 0)
		if cave.Voxels[0][500].IsSand {
			cave.Dump("debug-cave.txt")
			panic("Sand stuck")
		}
		if err != nil {
			panic(err)
		}
	}
}
