package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Monkey struct {
	Inspections    int
	Items          []int
	Operator       string
	OperationValue *int
	Test           struct {
		Value          int
		ThrowToIfTrue  int
		ThrowToIfFalse int
	}
}

func (m *Monkey) String() string {
	operationValue := "nil"
	if m.OperationValue != nil {
		operationValue = strconv.Itoa(*m.OperationValue)
	}
	return fmt.Sprintf("M(I:%s O:%s%s T(%d %d %d)",
		m.Items, m.Operator, operationValue,
		m.Test.Value, m.Test.ThrowToIfTrue, m.Test.ThrowToIfFalse,
	)
}

func ParseMonkey(scanner *bufio.Scanner) *Monkey {
	newMonkey := &Monkey{}
	for scanner.Scan() {
		line := scanner.Text()
		if strings.Index(line, "Starting items:") >= 0 {
			itemsPosition := strings.Index(line, "Starting items: ") + len("Starting items: ")
			itemsString := line[itemsPosition:]
			itemsAsString := strings.Split(itemsString, ", ")
			items := make([]int, len(itemsAsString))
			for i, itemString := range itemsAsString {
				item, err := strconv.Atoi(itemString)
				if err != nil {
					panic(err)
				}
				items[i] = item
			}
			newMonkey.Items = items
		} else if strings.Index(line, "Operation:") >= 0 {
			operationPos := strings.Index(line, "Operation: ") + len("Operation: new = old ")
			operationString := line[operationPos:]
			operationParts := strings.Split(operationString, " ")
			operator := operationParts[0]
			newMonkey.Operator = operator
			operationValue := operationParts[1]
			if operationValue == "old" {
				newMonkey.OperationValue = nil
			} else {
				value, err := strconv.Atoi(operationValue)
				if err != nil {
					panic(err)
				}
				newMonkey.OperationValue = &value
			}
		} else if strings.Index(line, "Test:") >= 0 {
			testValuePos := strings.Index(line, "Test: ") + len("Test: divisible by ")
			testValueString := line[testValuePos:]
			testValue, err := strconv.Atoi(testValueString)
			if err != nil {
				panic(err)
			}
			newMonkey.Test.Value = testValue
		} else if strings.Index(line, "If true:") >= 0 {
			throwIfTruePos := strings.Index(line, "If true: ") + len("If true: throw to monkey ")
			throwIfTrueString := line[throwIfTruePos:]
			throwIfTrue, err := strconv.Atoi(throwIfTrueString)
			if err != nil {
				panic(err)
			}
			newMonkey.Test.ThrowToIfTrue = throwIfTrue
		} else if strings.Index(line, "If false:") >= 0 {
			throwIfFalsePos := strings.Index(line, "If false: ") + len("If false: throw to monkey ")
			throwIfFalseString := line[throwIfFalsePos:]
			throwIfFalse, err := strconv.Atoi(throwIfFalseString)
			if err != nil {
				panic(err)
			}
			newMonkey.Test.ThrowToIfFalse = throwIfFalse
		} else if line == "" {
			break
		} else {
			panic(fmt.Errorf("unexpected line: %s", line))
		}
	}
	return newMonkey
}

func main() {
	inputFile, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer inputFile.Close()

	monkeys := []*Monkey{}
	scanner := bufio.NewScanner(inputFile)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.Index(line, "Monkey ") == 0 {
			monkey := ParseMonkey(scanner)
			monkeys = append(monkeys, monkey)
		}
	}

	fmt.Printf("Monkeys at start: %s\n", monkeys)

	commonModBase := 1
	for _, monkey := range monkeys {
		commonModBase *= monkey.Test.Value
	}

	// Part 1:
	// const rounds = 20
	// Part 2:
	const rounds = 10000

	for round := 1; round <= rounds; round++ {
		for _, monkey := range monkeys {
			oldItems := monkey.Items
			monkey.Items = make([]int, 0)
			for _, item := range oldItems {
				monkey.Inspections++
				operationValue := item
				if monkey.OperationValue != nil {
					operationValue = *monkey.OperationValue
				}
				if monkey.Operator == "*" {
					item *= operationValue
				} else if monkey.Operator == "+" {
					item += operationValue
				} else {
					panic(fmt.Errorf("unknown operator %s", monkey.Operator))
				}
				// Part 1: uncomment for Part 2
				// item /= 3

				// Part 2
				item = item % commonModBase

				var targetMonkey int
				if item%monkey.Test.Value == 0 {
					targetMonkey = monkey.Test.ThrowToIfTrue
				} else {
					targetMonkey = monkey.Test.ThrowToIfFalse
				}
				monkeys[targetMonkey].Items = append(monkeys[targetMonkey].Items, item)
			}
		}

		//fmt.Printf("After Round %d:\n", round)
		//for monkeyIndex, monkey := range monkeys {
		//	fmt.Printf("Monkey %d: %s\n", monkeyIndex, monkey.Items)
		//}
	}
	fmt.Printf("Inspections after %d rounds:\n", rounds)
	for monkeyIndex, monkey := range monkeys {
		fmt.Printf("Monkey %d: %d inspections\n", monkeyIndex, monkey.Inspections)
	}

	mostActive := make([]*Monkey, 2)
	for _, monkey := range monkeys {
		if mostActive[0] == nil || mostActive[0].Inspections < monkey.Inspections {
			mostActive[1] = mostActive[0]
			mostActive[0] = monkey
		} else if mostActive[1] == nil || mostActive[1].Inspections < monkey.Inspections {
			mostActive[1] = monkey
		}
	}
	monkeyBusiness := mostActive[0].Inspections * mostActive[1].Inspections
	fmt.Printf("Monkey business: %d\n", monkeyBusiness)
}
