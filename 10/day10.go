package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Command interface {
	CycleTime() int
	Execute(X int) int
}

type Noop struct {
}

func (n Noop) CycleTime() int {
	return 1
}

func (n Noop) Execute(X int) int {
	return X
}

type AddX struct {
	Value int
}

func (a AddX) CycleTime() int {
	return 2
}

func (a AddX) Execute(X int) int {
	return X + a.Value
}

func ParseCommand(commandString string) (Command, error) {
	if commandString == "noop" {
		return Noop{}, nil
	} else if strings.Index(commandString, "addx ") == 0 {
		value, err := strconv.Atoi(commandString[5:])
		if err != nil {
			return nil, err
		}
		return AddX{Value: value}, nil
	}
	return nil, fmt.Errorf("unknown command: '%s'", commandString)
}

func CalculateSignalStrength(cycleTime int, x int) int {
	return cycleTime * x
}

type Protocol struct {
	SignalStrengthSum int
}

func (p *Protocol) ReadSignal(cycleTime int, signalStrength int) {
	if cycleTime == 20 || cycleTime == 60 || cycleTime == 100 ||
		cycleTime == 140 || cycleTime == 180 || cycleTime == 220 {
		p.SignalStrengthSum += signalStrength
	}
}

func RenderPixel(cycleTime int, X int) {
	fmt.Print(".")
	pixelPosition := (cycleTime - 1) % 40
	if pixelPosition >= X-1 && pixelPosition <= X+1 {
		fmt.Print("#")
	} else {
		fmt.Print(".")
	}
	if (cycleTime)%40 == 0 {
		fmt.Println()
	}
}

func main() {
	inputFile, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer inputFile.Close()

	scanner := bufio.NewScanner(inputFile)
	cycleTime := 0
	X := 1
	signalStrength := 0
	protocol := Protocol{}
	for scanner.Scan() {
		line := scanner.Text()
		command, err := ParseCommand(line)
		if err != nil {
			panic(err)
		}
		for waitCycle := 0; waitCycle < command.CycleTime(); waitCycle++ {
			cycleTime++
			signalStrength = CalculateSignalStrength(cycleTime, X)
			protocol.ReadSignal(cycleTime, signalStrength)
			//fmt.Printf("Cycle %d: X = %d, SignalStrength = %d\n", cycleTime, X, signalStrength)
			RenderPixel(cycleTime, X)
		}
		X = command.Execute(X)
	}
	cycleTime++
	signalStrength = CalculateSignalStrength(cycleTime, X)
	protocol.ReadSignal(cycleTime, signalStrength)
	fmt.Printf("Cycle %d: X = %d\n", cycleTime, X)

	fmt.Printf("Signal Strength Sum: %d\n", protocol.SignalStrengthSum)
}
