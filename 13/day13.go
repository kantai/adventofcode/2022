package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
)

type AllPackets [][]any

func (a AllPackets) Len() int           { return len(a) }
func (a AllPackets) Less(i, j int) bool { return PacketIsLess(a[i], a[j]) }
func (a AllPackets) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }

func ParsePackets(text string, pos *int) ([]any, error) {
	if text[*pos] == '[' {
		var listElements []any
		var currentValue any
		currentValue = ""
		*pos++
		for ; *pos < len(text); *pos++ {
			if text[*pos] >= '0' && text[*pos] <= '9' {
				currentValue = currentValue.(string) + string(rune(text[*pos]))
				continue
			} else if text[*pos] == '[' {
				value, err := ParsePackets(text, pos)
				if err != nil {
					return nil, err
				}
				currentValue = value
			} else if text[*pos] == ',' {
				switch currentValue.(type) {
				case string:
					intValue, _ := strconv.Atoi(currentValue.(string))
					currentValue = intValue
				}
				listElements = append(listElements, currentValue)
				currentValue = ""
			} else if text[*pos] == ']' {
				if currentValue != "" {
					switch currentValue.(type) {
					case string:
						intValue, _ := strconv.Atoi(currentValue.(string))
						currentValue = intValue
					}
					listElements = append(listElements, currentValue)
				}
				return listElements, nil
			}
		}
	}
	return nil, fmt.Errorf("invalid input, missing ']'")
}

func PacketsInRightOrder(left []any, right []any) *bool {
	var result bool

	for index, leftValue := range left {
		if index >= len(right) {
			result = false
			return &result
		}
		rightValue := right[index]
		leftAsInt, leftIsInt := leftValue.(int)
		rightAsInt, rightIsInt := rightValue.(int)
		leftAsList, leftIsList := leftValue.([]any)
		rightAsList, rightIsList := rightValue.([]any)

		if leftIsList && rightIsInt {
			rightAsList = []any{rightAsInt}
			rightIsList = true
		}
		if leftIsInt && rightIsList {
			leftAsList = []any{leftAsInt}
			leftIsList = true
		}

		if leftIsList && rightIsList {
			tempResult := PacketsInRightOrder(leftAsList, rightAsList)
			if tempResult != nil {
				return tempResult
			}
		}

		if leftIsInt && rightIsInt {
			if leftAsInt < rightAsInt {
				result = true
				return &result
			} else if leftAsInt > rightAsInt {
				result = false
				return &result
			}
			continue
		}

	}

	if len(right) > len(left) {
		result = true
		return &result
	}

	return nil
}

func PacketIsLess(left []any, right []any) bool {
	result := PacketsInRightOrder(left, right)
	if result != nil {
		return *result
	}
	return false
}

func main() {
	inputFile, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer inputFile.Close()

	scanner := bufio.NewScanner(inputFile)

	pair := 1
	rightSum := 0
	var allPackets AllPackets
	for scanner.Scan() {
		line := scanner.Text()
		if line != "" {
			pos := 0
			leftPackets, err := ParsePackets(line, &pos)
			if err != nil {
				panic(err)
			}
			allPackets = append(allPackets, leftPackets)
			scanner.Scan()
			line := scanner.Text()

			pos = 0
			rightPackets, err := ParsePackets(line, &pos)
			if err != nil {
				panic(err)
			}
			allPackets = append(allPackets, rightPackets)
			result := PacketsInRightOrder(leftPackets, rightPackets)
			if result == nil {
				fmt.Printf("Pair %d neither right nor wrong order.\n", pair)
			} else if *result {
				fmt.Printf("Pair %d in right order.\n", pair)
				rightSum += pair
			} else if !*result {
				fmt.Printf("Pair %d in wrong order.\n", pair)
			}
			leftPackets = nil
			rightPackets = nil
			pair++
		}
	}
	fmt.Printf("Sum of right pairs %d.\n", rightSum)

	firstDividerIndex := 0
	firstDivider := []any{[]any{2}}
	secondDividerIndex := 0
	secondDivider := []any{[]any{6}}

	allPackets = append(allPackets, firstDivider)
	allPackets = append(allPackets, secondDivider)
	sort.Sort(allPackets)
	for index, packet := range allPackets {
		if PacketsInRightOrder(packet, firstDivider) == nil {
			firstDividerIndex = index + 1
		}
		if PacketsInRightOrder(packet, secondDivider) == nil {
			secondDividerIndex = index + 1
		}
		fmt.Printf("%s\n", packet)
	}

	fmt.Printf("Decoder key: %d\n", secondDividerIndex*firstDividerIndex)
}
