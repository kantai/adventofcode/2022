package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type Shape struct {
	Symbol string
	Score  int
	Name   string
}

func (s Shape) String() string {
	return s.Name
}

var Rock = Shape{
	Symbol: "A",
	Score:  1,
	Name:   "Rock",
}

var Paper = Shape{
	Symbol: "B",
	Score:  2,
	Name:   "Paper",
}

var Scissors = Shape{
	Symbol: "C",
	Score:  3,
	Name:   "Scissors",
}

var shapes = []*Shape{
	&Rock, &Paper, &Scissors,
}

type Result struct {
	Symbol string
	Score  int
}

var Lose = Result{
	Symbol: "X",
	Score:  0,
}

var Draw = Result{
	Symbol: "Y",
	Score:  3,
}

var Win = Result{
	Symbol: "Z",
	Score:  6,
}

var results = []*Result{
	&Lose, &Draw, &Win,
}

func ParseShape(symbol string) (*Shape, error) {
	for _, shape := range shapes {
		if shape.Symbol == symbol {
			return shape, nil
		}
	}
	return nil, fmt.Errorf("invalid symbol %s", symbol)
}

func ParseResult(symbol string) (*Result, error) {
	for _, result := range results {
		if result.Symbol == symbol {
			return result, nil
		}
	}
	return nil, fmt.Errorf("invalid symbol %s", symbol)
}

func CalculateResult(theirShape *Shape, ourShape *Shape) *Result {
	switch theirShape {
	case &Rock:
		switch ourShape {
		case &Rock:
			return &Draw
		case &Paper:
			return &Win
		case &Scissors:
			return &Lose
		}
	case &Paper:
		switch ourShape {
		case &Rock:
			return &Lose
		case &Paper:
			return &Draw
		case &Scissors:
			return &Win
		}
	case &Scissors:
		switch ourShape {
		case &Rock:
			return &Win
		case &Paper:
			return &Lose
		case &Scissors:
			return &Draw
		}
	}
	return &Lose
}

func CalculateShape(theirShape *Shape, expectedResult *Result) *Shape {
	switch theirShape {
	case &Rock:
		switch expectedResult {
		case &Draw:
			return &Rock
		case &Win:
			return &Paper
		case &Lose:
			return &Scissors
		}
	case &Paper:
		switch expectedResult {
		case &Lose:
			return &Rock
		case &Draw:
			return &Paper
		case &Win:
			return &Scissors
		}
	case &Scissors:
		switch expectedResult {
		case &Win:
			return &Rock
		case &Lose:
			return &Paper
		case &Draw:
			return &Scissors
		}
	}
	return &Rock
}

func main() {
	inputFile, err := os.Open("matchplan.txt")
	if err != nil {
		panic(err)
	}
	defer inputFile.Close()

	score := 0
	scanner := bufio.NewScanner(inputFile)
	for scanner.Scan() {
		line := scanner.Text()
		symbols := strings.Split(line, " ")
		opponentShape, err := ParseShape(symbols[0])
		if err != nil {
			panic(err)
		}
		expectedResult, err := ParseResult(symbols[1])
		if err != nil {
			panic(err)
		}
		ourShape := CalculateShape(opponentShape, expectedResult)
		result := CalculateResult(opponentShape, ourShape)
		if result != expectedResult {
			panic(fmt.Errorf("result not as expected."))
		}
		matchScore := result.Score
		fmt.Printf("%s vs. %s => %d\n", opponentShape, ourShape, matchScore)
		score += ourShape.Score + matchScore
	}
	fmt.Printf("Our final score %d\n", score)
}
