package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

type Valve struct {
	Name     string
	FlowRate int
	Tunnels  []*Valve
	IsOpen   bool
}

func CalculateDistances(valves map[string]*Valve) map[string]int {
	Distances := make(map[string]int)

	for _, valve := range valves {
		for _, otherValve := range valves {
			Distances[valve.Name+otherValve.Name] = 10000000
			Distances[otherValve.Name+valve.Name] = 10000000
		}
	}

	//for each edge (u, v) do
	//dist[u][v] ← w(u, v)  // The weight of the edge (u, v)
	for _, valve := range valves {
		for _, otherValve := range valve.Tunnels {
			Distances[valve.Name+otherValve.Name] = 1
			Distances[otherValve.Name+valve.Name] = 1
		}

		//for each vertex v do
		//dist[v][v] ← 0

		Distances[valve.Name+valve.Name] = 0
	}

	//for k from 1 to |V|
	//	for i from 1 to |V|
	//		for j from 1 to |V|
	//
	for _, kValve := range valves {
		for _, iValve := range valves {
			for _, jValve := range valves {

				//if dist[i][j] > dist[i][k] + dist[k][j]
				//	dist[i][j] ← dist[i][k] + dist[k][j]
				//end if
				if Distances[iValve.Name+jValve.Name] > Distances[iValve.Name+kValve.Name]+Distances[kValve.Name+jValve.Name] {
					Distances[iValve.Name+jValve.Name] = Distances[iValve.Name+kValve.Name] + Distances[kValve.Name+jValve.Name]
				}
			}
		}
	}
	//fmt.Printf("Distances %s", Distances)
	return Distances
}

type Simulation struct {
	CurrentValveMe       string
	CurrentValveElephant string
	OpenValves           []string
	ReleasedPressure     int
	MinutesLeftMe        int
	MinutesLeftElephant  int
	Actions              string
}

func (s *Simulation) PotentialPressureRelease(valves []Valve) int {
	potential := 0
	maxTime := s.MinutesLeftMe
	if maxTime < s.MinutesLeftElephant {
		maxTime = s.MinutesLeftElephant
	}
	for _, valve := range valves {
		if !s.IsOpen(valve.Name) {
			maxTime--
			potential += valve.FlowRate * maxTime
		}
	}
	return potential / 2
}

func (s *Simulation) OpenValve(valve string, flowRate int, isElephant bool) *Simulation {
	if valve != s.CurrentValveMe && !isElephant {
		panic("Setting wrong valve?")
	}
	if valve != s.CurrentValveElephant && isElephant {
		panic("Setting wrong valve?")
	}
	if s.Actions[len(s.Actions)-2:] != valve {
		//panic(fmt.Sprintf("Action %s, current valve %s", s.Actions, valve))
	}
	result := Simulation{
		OpenValves:           append(s.OpenValves, valve),
		Actions:              s.Actions + " ",
		CurrentValveMe:       s.CurrentValveMe,
		CurrentValveElephant: s.CurrentValveElephant,
	}
	if isElephant {
		result.ReleasedPressure = s.ReleasedPressure + flowRate*(s.MinutesLeftElephant-1)
		result.MinutesLeftElephant = s.MinutesLeftElephant - 1
		result.MinutesLeftMe = s.MinutesLeftMe
		result.Actions = s.Actions + "#"
	} else {
		result.ReleasedPressure = s.ReleasedPressure + flowRate*(s.MinutesLeftMe-1)
		result.MinutesLeftElephant = s.MinutesLeftElephant
		result.MinutesLeftMe = s.MinutesLeftMe - 1
		result.Actions = s.Actions + "."
	}
	return &result
}

func (s *Simulation) GoTo(valve string, distance int, isElephant bool) *Simulation {
	newOpenValves := make([]string, len(s.OpenValves))
	for i := 0; i < len(s.OpenValves); i++ {
		newOpenValves[i] = s.OpenValves[i]
	}
	result := Simulation{
		ReleasedPressure: s.ReleasedPressure,
		OpenValves:       newOpenValves,
		Actions:          s.Actions + valve,
	}
	if isElephant {
		result.MinutesLeftElephant = s.MinutesLeftElephant - distance
		result.MinutesLeftMe = s.MinutesLeftMe
		result.Actions = result.Actions
		result.CurrentValveMe = s.CurrentValveMe
		result.CurrentValveElephant = valve
	} else {
		result.MinutesLeftElephant = s.MinutesLeftElephant
		result.MinutesLeftMe = s.MinutesLeftMe - distance
		result.Actions = result.Actions
		result.CurrentValveMe = valve
		result.CurrentValveElephant = s.CurrentValveElephant
	}
	return &result
}

func (s *Simulation) IsOpen(valve string) bool {
	for _, valveName := range s.OpenValves {
		if valveName == valve {
			return true
		}
	}
	return false
}

func (s *Simulation) IsEnded(isElephant bool) bool {
	if isElephant {
		return s.MinutesLeftElephant <= 0
	} else {
		return s.MinutesLeftMe <= 0
	}
}

//
//func Simulate(simulation Simulation) *Simulation {
//
//	var bestSimulation *Simulation
//	var waitingSimulations = []*Simulation{&simulation}
//
//	for true {
//		if len(waitingSimulations) == 0 {
//			break
//		}
//		simulation := waitingSimulations[0]
//		waitingSimulations = waitingSimulations[1:]
//
//		fmt.Printf("Current simulation %s\n", simulation.Actions)
//
//		if simulation.IsEnded() {
//			if bestSimulation == nil || bestSimulation.ReleasedPressure < simulation.ReleasedPressure {
//				bestSimulation = simulation
//			}
//		} else {
//			if !simulation.IsOpen(simulation.CurrentValve) && simulation.CurrentValve.FlowRate > 0 {
//				newSimulation := simulation.OpenValve(valves[simulation.CurrentValve])
//				waitingSimulations = append(waitingSimulations, &newSimulation)
//			}
//			for _, otherValve := range simulation.CurrentValve.Tunnels {
//				newSimulation := simulation.GoTo(otherValve, 1)
//				waitingSimulations = append(waitingSimulations, &newSimulation)
//			}
//		}
//	}
//	return bestSimulation
//}

func main() {
	inputFile, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer inputFile.Close()

	valveRegEx, err := regexp.Compile("Valve (\\w+) has flow rate=(\\d+); tunnels? leads? to valves? (.+)$")
	if err != nil {
		panic(err)
	}

	scanner := bufio.NewScanner(inputFile)
	var startValve *Valve

	valves := make(map[string]*Valve)
	valvesList := make([]*Valve, 0)

	for scanner.Scan() {
		line := scanner.Text()

		matches := valveRegEx.FindAllStringSubmatch(line, len(line)+1)
		if len(matches) == 0 {
			fmt.Printf("No match in line: %s", line)
			panic("")
		}
		match := matches[0]
		valveName := match[1]
		flowRateAsString := match[2]
		flowRate, err := strconv.Atoi(flowRateAsString)
		if err != nil {
			panic(err)
		}
		tunnelsAsString := match[3]
		tunnelNames := strings.Split(tunnelsAsString, ", ")

		valve := &Valve{Name: valveName, FlowRate: flowRate}
		for _, name := range tunnelNames {
			otherValve, exists := valves[name]
			if exists {
				valve.Tunnels = append(valve.Tunnels, otherValve)
				otherValve.Tunnels = append(otherValve.Tunnels, valve)
			}
		}
		valves[valveName] = valve
		valvesList = append(valvesList, valve)
	}

	startValve = valves["AA"]

	//for name, valve := range valves {
	//fmt.Printf("Valve %s with rate %d and tunnels %s\n", name, valve.FlowRate, valve.Tunnels)
	//}

	distances := CalculateDistances(valves)
	minutes := 26

	//simulation := Simulation{MinutesLeft: 30, CurrentValve: startValve}
	//bestSimulation := Simulate(simulation)
	//
	//fmt.Printf("Best simulation: %s, %d", bestSimulation.Actions, bestSimulation.ReleasedPressure)

	var valvesWithPossibleFlow []Valve
	for _, valve := range valvesList {
		if valve.FlowRate > 0 {
			valvesWithPossibleFlow = append(valvesWithPossibleFlow, *valve)
		}
	}

	sort.Slice(valvesWithPossibleFlow, func(i, j int) bool {
		return valvesWithPossibleFlow[i].FlowRate > valvesWithPossibleFlow[j].FlowRate
	})

	//valveIndexes := make([]int, len(valvesWithPossibleFlow))
	//for i := 0; i < len(valvesWithPossibleFlow); i++ {
	//	valveIndexes[i] = i
	//}
	fmt.Printf("Valves with positive flow rate:\n")
	for _, v := range valvesWithPossibleFlow {
		fmt.Printf("%s %d\n", v.Name, v.FlowRate)
	}
	//permutations := permutations(valveIndexes)
	//fmt.Printf("Permutations: %d\n", len(permutations))

	//bestReleasedPressure := 0
	//for _, permutation := range permutations {
	//	minutesLeft := minutes
	//	currentValve := startValve
	//	releasedPressure := 0
	//	for _, valveIndex := range permutation {
	//		newValve := valvesWithPossibleFlow[valveIndex]
	//		minutesLeft -= distances[currentValve.Name+newValve.Name] + 1
	//		releasedPressure += newValve.FlowRate * minutesLeft
	//		currentValve = newValve
	//	}
	//	if minutesLeft < 0 {
	//		continue
	//	}
	//	if minutesLeft >= 0 && bestReleasedPressure < releasedPressure {
	//		bestReleasedPressure = releasedPressure
	//		fmt.Printf("New best: %d %s ", releasedPressure, startValve.Name)
	//		for _, valveIndex := range permutation {
	//			valve := valvesWithPossibleFlow[valveIndex]
	//			fmt.Printf(" -> %s", valve.Name)
	//		}
	//		fmt.Println()
	//	}
	//}

	// Part 1
	//var bestSimulation *Simulation
	//var waitingSimulations = []*Simulation{{
	//	CurrentValve: startValve.Name,
	//	MinutesLeft:  minutes,
	//}}
	//
	//for true {
	//	if len(waitingSimulations) == 0 {
	//		break
	//	}
	//	simulation := waitingSimulations[0]
	//	waitingSimulations = waitingSimulations[1:]
	//
	//	if simulation.IsEnded() {
	//		continue
	//	}
	//	//fmt.Printf("Current simulation %s %d %d\n", simulation.Actions, simulation.MinutesLeft, simulation.ReleasedPressure)
	//
	//	for i := 0; i < len(valvesWithPossibleFlow); i++ {
	//		valve := valvesWithPossibleFlow[i]
	//		if !simulation.IsOpen(valve.Name) {
	//			newSimulation := simulation.GoTo(valve.Name, distances[simulation.CurrentValve+valve.Name])
	//			if !newSimulation.IsEnded() {
	//				newSimulation = newSimulation.OpenValve(valve.Name, valve.FlowRate)
	//				if bestSimulation == nil || newSimulation.ReleasedPressure > bestSimulation.ReleasedPressure {
	//					bestSimulation = newSimulation
	//					fmt.Printf("New best: %d %s %d\n", bestSimulation.ReleasedPressure, bestSimulation.Actions, bestSimulation.MinutesLeft)
	//				}
	//				waitingSimulations = append(waitingSimulations, newSimulation)
	//			}
	//		}
	//	}
	//}
	//fmt.Printf("Overall best first run: %d %s ", bestSimulation.ReleasedPressure, bestSimulation.Actions)

	var bestSimulation *Simulation
	var waitingSimulations = []*Simulation{{
		CurrentValveMe:       startValve.Name,
		CurrentValveElephant: startValve.Name,
		MinutesLeftElephant:  minutes,
		MinutesLeftMe:        minutes,
	}}

	for true {
		if len(waitingSimulations) == 0 {
			break
		}
		simulation := waitingSimulations[0]
		waitingSimulations = waitingSimulations[1:]

		if strings.Index(simulation.Actions, "DD#JJ.HH#BB.") == 0 {
			fmt.Printf("Current simulation %s %d %d %d\n", simulation.Actions, simulation.MinutesLeftElephant, simulation.MinutesLeftMe, simulation.ReleasedPressure)
		}

		if simulation.MinutesLeftElephant == simulation.MinutesLeftMe {
			// Continue simulation twice, as elephant and as me
			isElephant := true
			if !simulation.IsEnded(isElephant) {
				bestSimulation, waitingSimulations = RunSimulation(simulation, valvesWithPossibleFlow, distances, isElephant, bestSimulation, waitingSimulations)
			}
			isElephant = false
			if !simulation.IsEnded(isElephant) {
				bestSimulation, waitingSimulations = RunSimulation(simulation, valvesWithPossibleFlow, distances, isElephant, bestSimulation, waitingSimulations)
			}
		} else {
			isElephant := false
			if simulation.MinutesLeftElephant > simulation.MinutesLeftMe {
				isElephant = true
			}
			if simulation.IsEnded(isElephant) {
				continue
			}
			bestSimulation, waitingSimulations = RunSimulation(simulation, valvesWithPossibleFlow, distances, isElephant, bestSimulation, waitingSimulations)

		}
	}
	//sim := &Simulation{
	//	CurrentValveMe:       startValve.Name,
	//	CurrentValveElephant: startValve.Name,
	//	MinutesLeftElephant:  minutes,
	//	MinutesLeftMe:        minutes,
	//}
	//sim = sim.GoTo("DD", distances["AADD"], true)
	//fmt.Printf("Current simulation %s %d %d %d\n", sim.Actions, sim.MinutesLeftElephant, sim.MinutesLeftMe, sim.ReleasedPressure)
	//sim = sim.OpenValve("DD", valves["DD"].FlowRate, true)
	//fmt.Printf("Current simulation %s %d %d %d\n", sim.Actions, sim.MinutesLeftElephant, sim.MinutesLeftMe, sim.ReleasedPressure)
	//sim = sim.GoTo("JJ", distances["AAJJ"], false)
	//fmt.Printf("Current simulation %s %d %d %d\n", sim.Actions, sim.MinutesLeftElephant, sim.MinutesLeftMe, sim.ReleasedPressure)
	//sim = sim.OpenValve("JJ", valves["JJ"].FlowRate, false)
	//fmt.Printf("Current simulation %s %d %d %d\n", sim.Actions, sim.MinutesLeftElephant, sim.MinutesLeftMe, sim.ReleasedPressure)
	//
	//sim = sim.GoTo("HH", distances["DDHH"], true)
	//fmt.Printf("Current simulation %s %d %d %d\n", sim.Actions, sim.MinutesLeftElephant, sim.MinutesLeftMe, sim.ReleasedPressure)
	//sim = sim.OpenValve("HH", valves["HH"].FlowRate, true)
	//fmt.Printf("Current simulation %s %d %d %d\n", sim.Actions, sim.MinutesLeftElephant, sim.MinutesLeftMe, sim.ReleasedPressure)
	//sim = sim.GoTo("BB", distances["JJBB"], false)
	//fmt.Printf("Current simulation %s %d %d %d\n", sim.Actions, sim.MinutesLeftElephant, sim.MinutesLeftMe, sim.ReleasedPressure)
	//sim = sim.OpenValve("BB", valves["BB"].FlowRate, false)
	//fmt.Printf("Current simulation %s %d %d %d\n", sim.Actions, sim.MinutesLeftElephant, sim.MinutesLeftMe, sim.ReleasedPressure)
	//
	//sim = sim.GoTo("EE", distances["HHEE"], true)
	//fmt.Printf("Current simulation %s %d %d %d\n", sim.Actions, sim.MinutesLeftElephant, sim.MinutesLeftMe, sim.ReleasedPressure)
	//sim = sim.OpenValve("EE", valves["EE"].FlowRate, true)
	//fmt.Printf("Current simulation %s %d %d %d\n", sim.Actions, sim.MinutesLeftElephant, sim.MinutesLeftMe, sim.ReleasedPressure)
	//sim = sim.GoTo("CC", distances["BBCC"], false)
	//fmt.Printf("Current simulation %s %d %d %d\n", sim.Actions, sim.MinutesLeftElephant, sim.MinutesLeftMe, sim.ReleasedPressure)
	//sim = sim.OpenValve("CC", valves["CC"].FlowRate, false)
	//fmt.Printf("Current simulation %s %d %d %d\n", sim.Actions, sim.MinutesLeftElephant, sim.MinutesLeftMe, sim.ReleasedPressure)
	//
	//fmt.Printf("In the end: %d %s ", sim.ReleasedPressure, sim.Actions)

	fmt.Printf("Overall best first run: %d %s ", (*bestSimulation).ReleasedPressure, (*bestSimulation).Actions)
}

func RunSimulation(simulation *Simulation, valvesWithPossibleFlow []Valve, distances map[string]int, isElephant bool, bestSimulation *Simulation, waitingSimulations []*Simulation) (*Simulation, []*Simulation) {
	//if strings.Index(simulation.Actions, "DD#JJ.HH#") == 0 {
	//	fmt.Printf("Current simulation %s %d %d %d\n", simulation.Actions, simulation.MinutesLeftElephant, simulation.MinutesLeftMe, simulation.ReleasedPressure)
	//}
	for i := 0; i < len(valvesWithPossibleFlow); i++ {
		valve := valvesWithPossibleFlow[i]
		if !simulation.IsOpen(valve.Name) {
			currentLocation := simulation.CurrentValveMe
			if isElephant {
				currentLocation = simulation.CurrentValveElephant
			}
			newSimulation := simulation.GoTo(valve.Name, distances[currentLocation+valve.Name], isElephant)
			if !newSimulation.IsEnded(isElephant) {
				newSimulation = newSimulation.OpenValve(valve.Name, valve.FlowRate, isElephant)
				if bestSimulation == nil || newSimulation.ReleasedPressure > bestSimulation.ReleasedPressure {
					bestSimulation = newSimulation
					fmt.Printf("New best: %d %s\n", bestSimulation.ReleasedPressure, bestSimulation.Actions)
				}
				if len(simulation.Actions) < 10 || bestSimulation.ReleasedPressure-simulation.ReleasedPressure < simulation.PotentialPressureRelease(valvesWithPossibleFlow) {
					newWaitingSimulations := append(waitingSimulations, newSimulation)
					waitingSimulations = newWaitingSimulations
				}
			}
		}
	}
	return bestSimulation, waitingSimulations
}

func permutations(arr []int) [][]int {
	var helper func([]int, int)
	res := [][]int{}

	helper = func(arr []int, n int) {
		if n == 1 {
			tmp := make([]int, len(arr))
			copy(tmp, arr)
			res = append(res, tmp)
		} else {
			for i := 0; i < n; i++ {
				helper(arr, n-1)
				if n%2 == 1 {
					tmp := arr[i]
					arr[i] = arr[n-1]
					arr[n-1] = tmp
				} else {
					tmp := arr[0]
					arr[0] = arr[n-1]
					arr[n-1] = tmp
				}
			}
		}
	}
	helper(arr, len(arr))
	return res
}
