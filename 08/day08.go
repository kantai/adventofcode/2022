package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

type Tree struct {
	Height             int32
	IsVisible          bool
	VisibleTreesTop    int
	VisibleTreesBottom int
	VisibleTreesLeft   int
	VisibleTreesRight  int
	ScenicScore        int
}

type Grid struct {
	Fields [][]*Tree
}

func (g Grid) String() string {
	result := ""
	for _, row := range g.Fields {
		for _, tree := range row {
			result += strconv.Itoa(int(tree.Height))
		}
		result += "\n"
	}
	return result
}

func (g Grid) PrintVisibleTrees() string {
	result := ""
	for _, row := range g.Fields {
		for _, tree := range row {
			if tree.IsVisible {
				fmt.Print("X")
			} else {
				fmt.Print("O")
			}
		}
		fmt.Print("\n")
	}
	return result
}

func (g Grid) PrintScenicScores() string {
	result := ""
	for _, row := range g.Fields {
		for _, tree := range row {
			fmt.Printf("%3d", tree.ScenicScore)
		}
		fmt.Print("\n")
	}
	return result
}

func (g *Grid) CountVisibleTrees() int {
	// 1. Count visible trees in rows from left to right
	for _, row := range g.Fields {
		maxHeight := int32(-1)
		for _, tree := range row {
			if tree.Height > maxHeight {
				tree.IsVisible = true
				maxHeight = tree.Height
			}
		}
	}

	// 2. Count visible trees in rows from right to left
	for _, row := range g.Fields {
		maxHeight := int32(-1)
		for x := len(row) - 1; x >= 0; x-- {
			tree := row[x]
			if tree.Height > maxHeight {
				tree.IsVisible = true
				maxHeight = tree.Height
			}
		}
	}

	// 3. Count visible trees in columns from top to bottom
	for x := 0; x < len(g.Fields[0]); x++ {
		maxHeight := int32(-1)
		for y := 0; y < len(g.Fields); y++ {
			tree := g.Fields[y][x]
			if tree.Height > maxHeight {
				tree.IsVisible = true
				maxHeight = tree.Height
			}
		}
	}

	// 4. Count visible trees in columns from bottom to top
	for x := 0; x < len(g.Fields[0]); x++ {
		maxHeight := int32(-1)
		for y := len(g.Fields) - 1; y >= 0; y-- {
			tree := g.Fields[y][x]
			if tree.Height > maxHeight {
				tree.IsVisible = true
				maxHeight = tree.Height
			}
		}
	}

	count := 0
	for _, row := range g.Fields {
		for _, tree := range row {
			if tree.IsVisible {
				count++
			}
		}
	}
	return count
}

func (g *Grid) CalculateScenicScore() int {
	maxScenicScore := 0
	for y, row := range g.Fields {
		for x, tree := range row {

			visibleTrees := 0
			for oy := y - 1; oy >= 0; oy-- {
				visibleTrees++
				if g.Fields[oy][x].Height >= tree.Height {
					break
				}
			}
			tree.VisibleTreesTop = visibleTrees

			visibleTrees = 0
			for oy := y + 1; oy < len(g.Fields); oy++ {
				visibleTrees++
				if g.Fields[oy][x].Height >= tree.Height {
					break
				}
			}
			tree.VisibleTreesBottom = visibleTrees

			visibleTrees = 0
			for ox := x - 1; ox >= 0; ox-- {
				visibleTrees++
				if g.Fields[y][ox].Height >= tree.Height {
					break
				}
			}
			tree.VisibleTreesLeft = visibleTrees

			visibleTrees = 0
			for ox := x + 1; ox < len(g.Fields[y]); ox++ {
				visibleTrees++
				if g.Fields[y][ox].Height >= tree.Height {
					break
				}
			}
			tree.VisibleTreesRight = visibleTrees

			tree.ScenicScore = tree.VisibleTreesLeft * tree.VisibleTreesRight * tree.VisibleTreesTop * tree.VisibleTreesBottom
			if tree.ScenicScore > maxScenicScore {
				maxScenicScore = tree.ScenicScore
			}
		}
	}
	return maxScenicScore
}

func ParseGridLine(line string, g *Grid) {
	row := make([]*Tree, len(line))
	for index, field := range line {
		row[index] = &Tree{
			Height: field - 48,
		}
	}
	g.Fields = append(g.Fields, row)
}

func main() {
	inputFile, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer inputFile.Close()

	scanner := bufio.NewScanner(inputFile)
	var grid Grid
	for scanner.Scan() {
		line := scanner.Text()

		ParseGridLine(line, &grid)
	}
	fmt.Printf("Grid: \n%s", grid)

	visibleTrees := grid.CountVisibleTrees()
	fmt.Printf("visible trees: %d\n", visibleTrees)
	//grid.PrintVisibleTrees()

	maxScenicScore := grid.CalculateScenicScore()
	fmt.Printf("max scenic score: %d\n", maxScenicScore)
	//grid.PrintScenicScores()

}
