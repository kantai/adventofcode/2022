package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type Materials struct {
	Ore      int
	Clay     int
	Obsidian int
	Geode    int
}

func (m Materials) AddMaterial(materialType string, amount int) *Materials {
	newMaterials := Materials{
		Ore:      m.Ore,
		Clay:     m.Clay,
		Obsidian: m.Obsidian,
		Geode:    m.Geode,
	}
	if materialType == "ore" {
		newMaterials.Ore += amount
	} else if materialType == "clay" {
		newMaterials.Clay += amount
	} else if materialType == "obsidian" {
		newMaterials.Obsidian += amount
	} else if materialType == "geode" {
		newMaterials.Geode += amount
	} else {
		panic(fmt.Errorf("unknown material %s", materialType))
	}
	return &newMaterials
}

func (m Materials) Covers(costs Materials) bool {
	return m.Ore >= costs.Ore && m.Clay >= costs.Clay && m.Obsidian >= costs.Obsidian && m.Geode >= costs.Geode
}

func (m Materials) Spend(costs Materials) *Materials {
	newMaterials := Materials{}
	newMaterials.Ore = m.Ore - costs.Ore
	newMaterials.Clay = m.Clay - costs.Clay
	newMaterials.Obsidian = m.Obsidian - costs.Obsidian
	newMaterials.Geode = m.Geode - costs.Geode
	return &newMaterials
}

type OreRobot struct {
}

type ClayRobot struct {
}

type ObsidianRobot struct {
}

type GeodeRobot struct {
}

type Blueprint struct {
	Id          int
	RobotPrices map[string]Materials
}

func (b *Blueprint) BuildableRobots(materials Materials) []string {
	result := make([]string, 0)
	for robotType, neededMaterials := range b.RobotPrices {
		if materials.Covers(neededMaterials) {
			result = append(result, robotType)
		}
	}
	return result
}

type Simulation struct {
	Blueprint          *Blueprint
	AvailableMaterials *Materials
	Robots             map[string]int
	Minute             int
	Actions            string
}

func (s *Simulation) TakeAction(action string) *Simulation {
	newRobots := map[string]int{
		"ore":      s.Robots["ore"],
		"clay":     s.Robots["clay"],
		"obsidian": s.Robots["obsidian"],
		"geode":    s.Robots["geode"],
	}
	newSimulation := Simulation{
		Blueprint:          s.Blueprint,
		AvailableMaterials: s.AvailableMaterials,
		Robots:             newRobots,
		Minute:             s.Minute,
		Actions:            s.Actions + "." + action,
	}
	for robotType, robots := range s.Robots {
		newSimulation.AvailableMaterials = newSimulation.AvailableMaterials.AddMaterial(robotType, robots)
	}
	if action == "pass" {

	} else {
		costs := s.Blueprint.RobotPrices[action]
		newSimulation.AvailableMaterials = newSimulation.AvailableMaterials.Spend(costs)
		newSimulation.Robots[action]++
	}
	newSimulation.Minute++
	return &newSimulation
}

func (s Simulation) QualityLevel() int {
	return s.Blueprint.Id * s.AvailableMaterials.Geode
}

func (s Simulation) Value() int {
	oreWeight := 1
	clayWeight := 2
	obsidianWeight := 3
	geodeWeight := 5

	oreValue := s.AvailableMaterials.Ore +
		s.Robots["ore"]*s.Blueprint.RobotPrices["ore"].Ore +
		s.Robots["ore"]*(24-s.Minute)
	clayValue := s.AvailableMaterials.Clay +
		s.Robots["clay"]*s.Blueprint.RobotPrices["clay"].Ore +
		s.Robots["clay"]*(24-s.Minute)
	obsidianValue := s.AvailableMaterials.Obsidian +
		s.Robots["obsidian"]*s.Blueprint.RobotPrices["obsidian"].Ore +
		clayWeight*(s.Robots["obsidian"]*s.Blueprint.RobotPrices["obsidian"].Clay) +
		s.Robots["obsidian"]*(24-s.Minute)
	geodeValue := s.AvailableMaterials.Geode +
		s.Robots["geode"]*s.Blueprint.RobotPrices["geode"].Ore +
		obsidianWeight*(s.Robots["geode"]*s.Blueprint.RobotPrices["geode"].Obsidian) +
		s.Robots["geode"]*(24-s.Minute)

	return oreValue*oreWeight +
		clayValue*clayWeight +
		obsidianValue*obsidianWeight +
		geodeValue*geodeWeight
}

func ParseBlueprint(line string) *Blueprint {
	parts := strings.Split(line, ": ")
	if len(parts) != 2 {
		panic(fmt.Errorf("invalid input %s", line))
	}

	blueprintId, err := strconv.Atoi(parts[0][10:])
	if err != nil {
		panic(err)
	}

	robotPattern, _ := regexp.Compile("Each (\\w+) robot costs (\\d+) (\\w+)(:? and (\\d+) (\\w+))?")

	robotCostsAsStrings := strings.Split(parts[1], ". ")

	blueprint := Blueprint{
		Id:          blueprintId,
		RobotPrices: make(map[string]Materials),
	}

	for _, robotCostString := range robotCostsAsStrings {
		materials := &Materials{}
		var robotType string
		matches := robotPattern.FindStringSubmatch(robotCostString)
		if len(matches) >= 4 {
			robotType = matches[1]
			materialAmountString := matches[2]
			materialAmount, err := strconv.Atoi(materialAmountString)
			if err != nil {
				panic(fmt.Errorf("invalid material amount %s", materialAmountString))
			}
			materialType := matches[3]
			materials = materials.AddMaterial(materialType, materialAmount)
		}
		if len(matches) == 7 && matches[5] != "" && matches[6] != "" {
			materialAmountString := matches[5]
			materialAmount, err := strconv.Atoi(materialAmountString)
			if err != nil {
				panic(fmt.Errorf("invalid material amount %s", materialAmountString))
			}
			materialType := matches[6]
			materials = materials.AddMaterial(materialType, materialAmount)
		}
		blueprint.RobotPrices[robotType] = *materials
	}
	return &blueprint
}

func main() {
	inputFile, err := os.Open("test.txt")
	if err != nil {
		panic(err)
	}
	defer inputFile.Close()

	scanner := bufio.NewScanner(inputFile)
	blueprints := make(map[int]*Blueprint, 0)
	for scanner.Scan() {
		line := scanner.Text()
		blueprint := ParseBlueprint(line)
		blueprints[blueprint.Id] = blueprint
		fmt.Printf("New blueprint: %s\n", blueprint)
	}

	startingMaterials := Materials{Ore: 1}

	startingSimulation := Simulation{
		AvailableMaterials: &startingMaterials,
		Robots: map[string]int{
			"ore":      1,
			"clay":     0,
			"obsidian": 0,
			"geode":    0,
		},
		Minute:  0,
		Actions: "",
	}
	simulations := make([]*Simulation, 0)
	bestSimulationByBlueprint := map[int]*Simulation{}

	for _, blueprint := range blueprints {
		blueprintSimulation := startingSimulation
		blueprintSimulation.Blueprint = blueprint
		simulations = append(simulations, &blueprintSimulation)
		bestSimulationByBlueprint[blueprint.Id] = &blueprintSimulation
	}

	simulationSteps := 0
	for {
		if len(simulations) == 0 {
			break
		}
		currentSimulation := simulations[0]
		simulations = simulations[1:]
		simulationSteps++
		if simulationSteps%10000 == 0 {
			fmt.Printf("Simulations, steps %d in queue %d, minute %d\n", simulationSteps, len(simulations), currentSimulation.Minute)
		}

		blueprint := currentSimulation.Blueprint
		buildableRobots := blueprint.BuildableRobots(*currentSimulation.AvailableMaterials)
		possibleActions := make([]string, len(buildableRobots)+1)
		possibleActions[0] = "pass"
		for index, robotType := range buildableRobots {
			possibleActions[index+1] = robotType
		}

		for _, action := range possibleActions {
			newSimulation := currentSimulation.TakeAction(action)
			//qualityLevel := newSimulation.QualityLevel()
			//if qualityLevel > bestSimulationByBlueprint[newSimulation.Blueprint.Id].QualityLevel() {
			value := newSimulation.Value()
			if value > bestSimulationByBlueprint[newSimulation.Blueprint.Id].Value() {
				//fmt.Printf("Found new best %s\n", newSimulation)
				bestSimulationByBlueprint[newSimulation.Blueprint.Id] = newSimulation
			}
			hasObsidianRobot := newSimulation.Robots["obsidian"] > 0
			if !hasObsidianRobot && newSimulation.Minute >= 12 {
				continue
			}
			if newSimulation.Minute > 9 && newSimulation.Value() < bestSimulationByBlueprint[newSimulation.Blueprint.Id].Value()*3/4 {
				continue
			}
			if newSimulation.Minute < 24 {
				simulations = append(simulations, newSimulation)
			}
		}
	}
	totalQualityLevel := 0
	for _, blueprint := range blueprints {
		totalQualityLevel += bestSimulationByBlueprint[blueprint.Id].QualityLevel()
	}

	fmt.Printf("Total quality level %d\n", totalQualityLevel)
}
