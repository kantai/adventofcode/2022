package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

type FoodItem struct {
	Calories uint
}

type Elf struct {
	Items []FoodItem
}

func (e *Elf) TotalCalories() uint {
	var totalCalories uint
	for _, item := range e.Items {
		totalCalories += item.Calories
	}
	return totalCalories
}

func main() {
	inputFile, err := os.Open("elves.txt")
	if err != nil {
		panic(err)
	}
	defer inputFile.Close()

	elves := make([]*Elf, 0, 10)
	scanner := bufio.NewScanner(inputFile)
	elves = append(elves, &Elf{})
	for scanner.Scan() {
		line := scanner.Text()
		if line == "" {
			elves = append(elves, &Elf{})
			fmt.Printf("Found a new elf. Currently %d\n", len(elves))
		} else {
			calories, err := strconv.Atoi(line)
			if err != nil {
				panic(err)
			}
			elves[len(elves)-1].Items = append(elves[len(elves)-1].Items, FoodItem{Calories: uint(calories)})
		}
	}

	elfWithMostCalories := elves[0]
	for index, elf := range elves {
		fmt.Printf("Elf %d transports %d calories.\n", index, elf.TotalCalories())
		if elf.TotalCalories() > elfWithMostCalories.TotalCalories() {
			elfWithMostCalories = elf
		}
	}
	fmt.Printf("Elf with most calories: %d calories.\n", elfWithMostCalories.TotalCalories())

	var topThreeElves [3]*Elf
	for _, elf := range elves {
		currentTotalCalories := elf.TotalCalories()
		if topThreeElves[0] == nil || topThreeElves[0].TotalCalories() < currentTotalCalories {
			topThreeElves[2] = topThreeElves[1]
			topThreeElves[1] = topThreeElves[0]
			topThreeElves[0] = elf
		} else if topThreeElves[1] == nil || topThreeElves[1].TotalCalories() < currentTotalCalories {
			topThreeElves[2] = topThreeElves[1]
			topThreeElves[1] = elf
		} else if topThreeElves[2] == nil || topThreeElves[2].TotalCalories() < currentTotalCalories {
			topThreeElves[2] = elf
		}
	}

	topThreeCalories := topThreeElves[0].TotalCalories() + topThreeElves[1].TotalCalories() + topThreeElves[2].TotalCalories()
	fmt.Printf("Top three elves are carrying %d calories.\n", topThreeCalories)
}
