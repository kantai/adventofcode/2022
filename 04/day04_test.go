package main

import (
	"testing"
)

func TestSectionRangesDontOverlap(t *testing.T) {
	sr1 := SectionRange{Begin: 2, End: 4}
	sr2 := SectionRange{Begin: 5, End: 7}
	if sr1.Overlaps(sr2) {
		t.Fatalf("Sections sr1 %s and sr2 %s shouldn't overlap.", sr1, sr2)
	}
	if sr2.Overlaps(sr1) {
		t.Fatalf("Sections sr2 %s and sr1 %s shouldn't overlap.", sr2, sr1)
	}
}

func TestSectionRangesOverlap(t *testing.T) {
	sr1 := SectionRange{Begin: 2, End: 5}
	sr2 := SectionRange{Begin: 4, End: 7}
	if !sr1.Overlaps(sr2) {
		t.Fatalf("Sections sr1 %s and sr2 %s should overlap.", sr1, sr2)
	}
	if !sr2.Overlaps(sr1) {
		t.Fatalf("Sections sr2 %s and sr1 %s should overlap.", sr2, sr1)
	}
}

func TestSectionRangesOverlapFully(t *testing.T) {
	sr1 := SectionRange{Begin: 2, End: 9}
	sr2 := SectionRange{Begin: 4, End: 7}
	if !sr1.Overlaps(sr2) {
		t.Fatalf("Sections sr1 %s and sr2 %s should overlap.", sr1, sr2)
	}
	if !sr2.Overlaps(sr1) {
		t.Fatalf("Sections sr2 %s and sr1 %s should overlap.", sr2, sr1)
	}
}
