# Advent of Code 2022

This repository contains some solutions for the 2022's [advent of code](https://adventofcode.com/2022)
implemented in Go. The programming of those solutions is streamed every day at 6:30 CET on twitch.tv
in the stream [kantailive](https://www.twitch.tv/kantailive). The new solution will be added every
day after the stream happened.

## License
Licensed under MIT license.

