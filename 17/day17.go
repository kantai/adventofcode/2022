package main

import (
	"fmt"
	"os"
)

type Rock struct {
	X      int
	Y      int
	Width  int
	Height int
	fill   [][]bool
}

func CreateRock(number int, height int) *Rock {
	rock := &Rock{
		X: 2,
		Y: height,
	}
	switch number % 5 {
	// Horizontal line
	case 0:
		rock.Height = 1
		rock.Width = 4
		rock.fill = [][]bool{{true, true, true, true}}
		break
	// Plus
	case 1:
		rock.Height = 3
		rock.Width = 3
		rock.fill = [][]bool{{false, true, false}, {true, true, true}, {false, true, false}}
		break
	// Corner
	case 2:
		rock.Height = 3
		rock.Width = 3
		rock.fill = [][]bool{{true, true, true}, {false, false, true}, {false, false, true}}
		break
	// Vertical line
	case 3:
		rock.Height = 4
		rock.Width = 1
		rock.fill = [][]bool{{true}, {true}, {true}, {true}}
		break
	// Vertical line
	case 4:
		rock.Height = 2
		rock.Width = 2
		rock.fill = [][]bool{{true, true}, {true, true}}
		break
	}
	return rock
}

type Cave struct {
	Rows   [][]*Rock
	Height int
	Width  int
}

func (c *Cave) MoveDown(rock *Rock) bool {
	if rock.Y > c.Height {

	} else {
		for y := rock.Y; y < rock.Y+rock.Height; y++ {
			for x := rock.X; x < rock.X+rock.Width; x++ {
				m := x - rock.X
				n := y - rock.Y
				if y == 0 || (y-1 < c.Height && rock.fill[n][m] && c.Rows[y-1][x] != nil) {
					return true
				}
			}
		}
	}
	rock.Y--
	return false
}

func (c *Cave) MoveRight(rock *Rock) {
	if rock.X+rock.Width-1 != c.Width-1 {

		canMove := true
		if rock.Y > c.Height {

		} else {
			for y := rock.Y; y < rock.Y+rock.Height; y++ {
				for x := rock.X; x < rock.X+rock.Width; x++ {
					m := x - rock.X
					n := y - rock.Y
					if y < c.Height && rock.fill[n][m] && c.Rows[y][x+1] != nil {
						canMove = false
					}
				}
			}
		}
		if canMove {
			rock.X++
		}
	}
}

func (c *Cave) MoveLeft(rock *Rock) {
	if rock.X > 0 {

		canMove := true
		if rock.Y > c.Height {

		} else {
			for y := rock.Y; y < rock.Y+rock.Height; y++ {
				for x := rock.X; x < rock.X+rock.Width; x++ {
					m := x - rock.X
					n := y - rock.Y
					if y < c.Height && rock.fill[n][m] && c.Rows[y][x-1] != nil {
						canMove = false
					}
				}
			}
		}
		if canMove {
			rock.X--
		}
	}
}

func (c *Cave) Print(rock *Rock) {
	fmt.Println("---------------------------------")
	height := c.Height
	if height < rock.Y+rock.Height-1 {
		height = rock.Y + rock.Height - 1
	}

	for y := height; y >= 0; y-- {
		fmt.Printf("%3d", y)
		for x := 0; x < c.Width; x++ {
			m := x - rock.X
			n := y - rock.Y
			if m >= 0 && m < rock.Width &&
				n >= 0 && n < rock.Height &&
				rock.fill[n][m] {
				fmt.Print("@")
			} else {
				if y < c.Height && c.Rows[y][x] != nil {
					fmt.Print("#")
				} else {
					fmt.Print(".")
				}
			}
		}
		fmt.Println("")
	}
}

func (c *Cave) ApplyJets(rock *Rock, jetIndex int, jetPatterns string) {
	direction := rune(jetPatterns[jetIndex%len(jetPatterns)])
	if direction == '>' {
		//fmt.Println("Move to the right")
		c.MoveRight(rock)
	} else if direction == '<' {
		//fmt.Println("Move to the left")
		c.MoveLeft(rock)
	}
}

func main() {

	jetPatternBytes, err := os.ReadFile("input.txt")
	if err != nil {
		panic(err)
	}
	jetPatterns := string(jetPatternBytes)

	cave := Cave{
		Width:  7,
		Height: 0,
	}
	step := 0

	for i := 0; i < 2022; i++ {
		rock := CreateRock(i, cave.Height+3)
		//cave.Print(rock)
		for true {
			cave.ApplyJets(rock, step, jetPatterns)
			step++
			//time.Sleep(time.Second)

			fixed := cave.MoveDown(rock)
			if fixed {
				newHeight := rock.Y + rock.Height
				if newHeight > cave.Height {
					for y := cave.Height; y < newHeight; y++ {
						cave.Rows = append(cave.Rows, make([]*Rock, cave.Width))
					}
					cave.Height = newHeight
				}
				for y := rock.Y; y < rock.Y+rock.Height; y++ {
					for x := rock.X; x < rock.X+rock.Width; x++ {
						m := x - rock.X
						n := y - rock.Y
						if rock.fill[n][m] {
							cave.Rows[y][x] = rock
						}
					}
				}
				break
			}
		}
	}

	fmt.Printf("Height of cave at the end: %d\n", cave.Height)
}
