package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Item interface {
	ParentDirectory() *Directory
	Name() string
	Size() int
}

type File struct {
	parent *Directory
	name   string
	size   int
}

func (f File) ParentDirectory() *Directory {
	return f.parent
}

func (f File) Name() string {
	return f.name
}

func (f File) Size() int {
	return f.size
}

func (f File) String() string {
	return f.name
}

type Directory struct {
	parent *Directory
	name   string
	Items  []Item
}

func (d *Directory) ParentDirectory() *Directory {
	return d.parent
}

func (d *Directory) Name() string {
	return d.name
}

func (d *Directory) Size() int {
	totalSize := 0
	for _, item := range d.Items {
		totalSize += item.Size()
	}
	return totalSize
}

func (d *Directory) String() string {
	return d.name
}

func (d *Directory) FindSubDirectory(name string) *Directory {
	for _, item := range d.Items {
		if name == item.Name() {
			directory, ok := item.(*Directory)
			if !ok {
				panic(fmt.Errorf("%s is not a directory", item.Name()))
			}
			return directory
		}
	}
	return nil
}

func (d *Directory) PrintDirectoriesUnder100k(indent string) int {
	totalSize := 0
	if d.Size() <= 100000 {
		fmt.Printf("%s%s %d\n", indent, d, d.Size())
		totalSize += d.Size()
	}
	for _, item := range d.Items {
		subDirectory, ok := item.(*Directory)
		if ok {
			totalSize += subDirectory.PrintDirectoriesUnder100k("  " + indent)
		}
	}
	return totalSize
}

func ParseCommand(commandLine string, currentDirectory *Directory) *Directory {
	if commandLine[:2] == "cd" {
		directoryName := commandLine[3:]
		if directoryName == "/" {
			return &Directory{name: "/"}
		} else if directoryName == ".." {
			return currentDirectory.ParentDirectory()
		} else {
			subDirectory := currentDirectory.FindSubDirectory(directoryName)
			if subDirectory == nil {
				panic(fmt.Errorf("no subdirectory %s", directoryName))
			}
			return subDirectory
		}

	} else if commandLine[:2] == "ls" {

	}

	return currentDirectory
}

func ParseItem(line string, currentDirectory *Directory) Item {
	parts := strings.Split(line, " ")
	if len(parts) != 2 {
		panic(fmt.Errorf("no valid file info, %s", line))
	}
	name := parts[1]
	if parts[0] == "dir" {
		return &Directory{name: name, parent: currentDirectory}
	} else {
		size, err := strconv.Atoi(parts[0])
		if err != nil {
			panic(err)
		}
		return &File{name: name, size: size, parent: currentDirectory}
	}
}

func main() {
	inputFile, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer inputFile.Close()

	scanner := bufio.NewScanner(inputFile)
	var directory *Directory
	var rootDirectory *Directory
	allDirectories := make([]*Directory, 0)
	for scanner.Scan() {
		line := scanner.Text()
		if rune(line[0]) == '$' {
			newDirectory := ParseCommand(line[2:], directory)
			if newDirectory != directory {
				if directory == nil {
					rootDirectory = newDirectory
				}
				directory = newDirectory
				exists := false
				for _, existingDirectory := range allDirectories {
					if existingDirectory == newDirectory {
						exists = true
					}
				}
				if !exists {
					allDirectories = append(allDirectories, newDirectory)
				}
				fmt.Printf("Changed directory to %s\n", directory)
			}
		} else {
			item := ParseItem(line, directory)
			directory.Items = append(directory.Items, item)
			fmt.Printf("Added item %s to directory %s\n", item, directory)
		}
	}
	totalSize := rootDirectory.PrintDirectoriesUnder100k("  ")

	fmt.Printf("Total size of small directories: %d\n", totalSize)
	fmt.Printf("Root directory items %s\n", rootDirectory.Items)
	fmt.Printf("Root directory size %s\n", rootDirectory.Size())

	totalDiskSpace := 70000000
	requiredFreeDiskSpace := 30000000
	freeDiskSpace := totalDiskSpace - rootDirectory.Size()
	needToFreeDiskSpace := requiredFreeDiskSpace - freeDiskSpace

	fmt.Printf("Need to free atleast %d\n", needToFreeDiskSpace)

	var toDelete *Directory
	freeSpaceAfterDeletion := 99999999999999
	for _, dir := range allDirectories {
		freedUpDiskSpace := dir.Size() - needToFreeDiskSpace
		if freedUpDiskSpace >= 0 && freedUpDiskSpace < freeSpaceAfterDeletion {
			toDelete = dir
			freeSpaceAfterDeletion = freedUpDiskSpace
		}
	}
	fmt.Printf("Deleting directory %s would free %d\n", toDelete, toDelete.Size())
}
