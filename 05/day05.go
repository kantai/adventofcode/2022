package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type Stack struct {
	Crates []string
	Name   string
}

type Cargo struct {
	Stacks []*Stack
}

func (c *Cargo) GetStack(stackNumber int) *Stack {
	existingStacks := len(c.Stacks)
	if stackNumber >= existingStacks {
		neededStacks := stackNumber - existingStacks + 1
		for i := existingStacks; i < existingStacks+neededStacks; i++ {
			c.Stacks = append(c.Stacks, &Stack{
				Name: strconv.Itoa(i + 1),
			})
		}
	}
	return c.Stacks[stackNumber]
}

func (c *Cargo) GetStackByName(stackName string) *Stack {
	for _, stack := range c.Stacks {
		if stack.Name == stackName {
			return stack
		}
	}
	return nil
}

func (c *Cargo) MoveCrates(move Move) {
	fromStack := c.GetStackByName(move.FromStack)
	toStack := c.GetStackByName(move.ToStack)
	// Part 1
	//for i := 0; i < move.CrateCount; i++ {
	//	movingCrates := make([]string, 1)
	//	copy(movingCrates, fromStack.Crates[:1])
	//	fromStack.Crates = fromStack.Crates[1:]
	//	toStack.Crates = append(movingCrates, toStack.Crates...)
	//}

	// Part 2
	movingCrates := make([]string, move.CrateCount)
	copy(movingCrates, fromStack.Crates[:move.CrateCount])
	fromStack.Crates = fromStack.Crates[move.CrateCount:]
	toStack.Crates = append(movingCrates, toStack.Crates...)
}

func IsCrate(text string) bool {
	if len(text) != 3 {
		return false
	}
	if rune(text[0]) == rune("["[0]) && rune(text[2]) == rune("]"[0]) {
		return true
	}
	return false
}

type Move struct {
	CrateCount int
	FromStack  string
	ToStack    string
}

func (m Move) String() string {
	return fmt.Sprintf("%d crates from %s to %s", m.CrateCount, m.FromStack, m.ToStack)
}

func ParseMove(line string) (Move, error) {
	pattern := "move (\\d+) from (\\d+) to (\\d+)"
	regex, err := regexp.Compile(pattern)
	if err != nil {
		return Move{}, err
	}
	matches := regex.FindStringSubmatch(line)
	if matches != nil && len(matches) == 4 {
		crateCount, err := strconv.Atoi(matches[1])
		if err != nil {
			return Move{}, err
		}
		fromStack := matches[2]
		toStack := matches[3]
		result := Move{
			CrateCount: crateCount,
			FromStack:  fromStack,
			ToStack:    toStack,
		}
		return result, nil
	}
	return Move{}, fmt.Errorf("no valid move found")
}

func main() {
	inputFile, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer inputFile.Close()

	isParsingCrates := true
	isParsingMoves := false
	cargo := Cargo{
		Stacks: make([]*Stack, 0),
	}
	scanner := bufio.NewScanner(inputFile)
	for scanner.Scan() {
		line := scanner.Text()
		if isParsingCrates {
			if line == "" {
				isParsingCrates = false
				isParsingMoves = true
				fmt.Printf("Cargo before moves %s\n", cargo)
			}
			runes := []rune(line)
			currentCrate := ""
			stackNumber := 0
			for i, r := range runes {
				stackNumber = i / 4
				if (i % 4) == 3 {
					currentCrate = strings.Trim(currentCrate, " ")
					if currentCrate != "" {
						stack := cargo.GetStack(stackNumber)
						if IsCrate(currentCrate) {
							stack.Crates = append(stack.Crates, currentCrate)
						} else {
							stack.Name = currentCrate
						}
					}
					currentCrate = ""
				} else {
					currentCrate += string(r)
				}
			}
			currentCrate = strings.Trim(currentCrate, " ")
			if currentCrate != "" {
				stack := cargo.GetStack(stackNumber)
				if IsCrate(currentCrate) {
					stack.Crates = append(stack.Crates, currentCrate)
				} else {
					stack.Name = currentCrate
				}
			}
		} else if isParsingMoves {
			move, err := ParseMove(line)
			if err != nil {
				panic(err)
			}
			fmt.Printf("parsed move %s\n", move)
			cargo.MoveCrates(move)
			fmt.Printf("Cargo after move %s: %s\n", move, cargo)
		}
	}

	fmt.Printf("Cargo after moves %s\n", cargo)
}
